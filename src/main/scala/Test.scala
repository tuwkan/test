
import java.sql.Timestamp

import scala.annotation.tailrec
import scala.collection.mutable

object Test {

  private val LIMIT = 4 // Сколько элементов за один запрос тянем из базы

  private val table: List[(Long, Timestamp)] = List(
    (1L, Timestamp.valueOf("2016-09-11 00:00:00")),
    (2L, Timestamp.valueOf("2016-09-12 00:00:00")),
    (3L, Timestamp.valueOf("2016-09-13 00:00:00")),
    (4L, Timestamp.valueOf("2016-09-14 00:00:00")),
    (5L, Timestamp.valueOf("2016-09-09 00:00:00")),
    (6L, Timestamp.valueOf("2016-09-08 00:00:00")),
    (7L, Timestamp.valueOf("2016-09-15 00:00:00"))
  )

  def main(args: Array[String]): Unit = {

    findRetroactively()
  }


  def findRetroactively(): Set[Long] = {

    @tailrec
    def rec(timestamp: Timestamp, data: List[(Long, Timestamp)], offset: Int, acc: mutable.Set[Long]): Set[Long] = {

      if (data.isEmpty) {
        acc.toSet
      } else {
        val maximum = data.foldLeft(timestamp)((max, value) => {
          if (value._2.before(max)) {
            acc += value._1
            max
          } else {
            value._2
          }
        })
        rec(maximum, getDataFromDataBase(offset, LIMIT), offset + LIMIT, acc)
      }
    }

    val init = getDataFromDataBase(0, LIMIT)

    init.headOption match {
      case Some(value) => rec(value._2, init, LIMIT, mutable.Set())
      case None => Set[Long]()
    }
  }

  // SELECT id, time FROM someTable ORDER BY id OFFSET $offset LIMIT $limit;
  def getDataFromDataBase(offset: Int, limit: Int): List[(Long, Timestamp)] = {
    table.slice(offset, offset + LIMIT)
  }

}